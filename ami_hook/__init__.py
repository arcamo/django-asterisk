from django import dispatch

default_app_config = 'ami_hook.apps.AmiHookConfig'

model_pre_save = dispatch.Signal(providing_args=['instance', 'form'])
model_post_save = dispatch.Signal(providing_args=['instance', 'form'])
