from django.apps import apps
from django.db.models.signals import post_delete
from django.db.utils import OperationalError
from django.dispatch import receiver
from . import model_post_save
from .models import Hook

ami_client = apps.get_app_config('ami_hook').ami_client


def prepare_hook(hook):
    listener = ami_client.add_event_listener(
        hook.build_callable(),
        white_list=hook.get_white_list(),
        black_list=hook.get_black_list()
    )
    return hook.pk, listener


try:
    event_listeners = dict(map(prepare_hook, Hook.objects.all() or []))
except OperationalError:
    pass


@receiver(model_post_save, sender=Hook)
def post_save_hook(instance, **kwargs):
    if instance.pk in event_listeners:
        listener = event_listeners.pop(instance.pk)
        ami_client.remove_event_listener(listener)
    pk, listener = prepare_hook(instance)
    event_listeners[pk] = listener


@receiver(post_delete, sender=Hook)
def post_delete_hook(instance, **kwargs):
    if instance.pk in event_listeners:
        listener = event_listeners.pop(instance.pk)
        ami_client.remove_event_listener(listener)
