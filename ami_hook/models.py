# -*- encoding: utf-8 -*-
from __future__ import unicode_literals

import json

import jsonfield
import requests_queue
from django.db import models
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _


def send_post(url, headers, body, ami_event):
    event = {
        'name': ami_event.name,
        'data': ami_event.keys,
        'time': timezone.now().isoformat()
    }
    headers['Content-Type'] = 'application/json; charset=utf-8'
    body['event'] = event
    requests_queue.post(url, raw_body=json.dumps(body), headers=headers)


@python_2_unicode_compatible
class AMIEvent(models.Model):
    class Meta:
        verbose_name = _('Evento AMI')
        verbose_name_plural = _('Eventos AMI')
        ordering = ['name']

    name = models.CharField(
        max_length=128,
        verbose_name=_('Nome'),
        unique=True
    )

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Hook(models.Model):
    class Meta:
        verbose_name = _('Web Hook')
        verbose_name_plural = _('Web Hooks')

    url = models.URLField(
        blank=False,
        null=False,
        verbose_name=_('Destino')
    )

    header = jsonfield.JSONField(
        blank=True,
        null=True,
        verbose_name=_('Dados do Cabeçario'),
        help_text=_('Dados para serem inseridos no cabeçario HTTP. Formato JSON')
    )

    body = jsonfield.JSONField(
        blank=True,
        null=True,
        verbose_name=_('Dados do Corpo'),
        help_text=_('Dados para serem inseridos no corpo HTTP. Formato JSON')
    )

    white_list = models.ManyToManyField(
        AMIEvent,
        blank=True,
        verbose_name=_('WhiteList'),
        related_name='white_hook',
    )

    def get_white_list(self):
        if self.white_list.count() < 1:
            return None
        return map(lambda e: e.name, self.white_list.all())

    black_list = models.ManyToManyField(
        AMIEvent,
        blank=True,
        verbose_name=_('BlackList'),
        related_name='black_hook',
    )

    def get_black_list(self):
        return map(lambda e: e.name, self.black_list.all())

    def build_callable(self):
        url = self.url
        header = self.header or {}
        body = self.body or {}

        def callable(event, **kwargs):
            send_post(url, header, body, event)

        return callable

    def __str__(self):
        return self.url
