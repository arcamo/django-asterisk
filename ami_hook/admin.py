from django.contrib import admin
from django.forms import models
from . import model_post_save, model_pre_save
from .models import Hook, AMIEvent

base_model_form_save = models.BaseModelForm.save

base_model_form_save_m2m = models.BaseModelForm._save_m2m


def form_save_model(self, *args, **kwargs):
    instance = self.instance
    model_pre_save.send(sender=instance.__class__, instance=instance, form=self)
    result = base_model_form_save(self, *args, **kwargs)
    return result


def form_save_m2m_model(self, *args, **kwargs):
    instance = self.instance
    result = base_model_form_save_m2m(self, *args, **kwargs)
    model_post_save.send(sender=instance.__class__, instance=instance, form=self)
    return result


models.BaseModelForm.save = form_save_model
models.BaseModelForm._save_m2m = form_save_m2m_model


@admin.register(Hook)
class HookAdmin(admin.ModelAdmin):
    filter_horizontal = ['black_list', 'white_list']


@admin.register(AMIEvent)
class AMIEventAdmin(admin.ModelAdmin):
    pass
