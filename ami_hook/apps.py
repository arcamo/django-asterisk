from __future__ import unicode_literals

import logging

from six import text_type

from asterisk.ami import AMIClient
from django.apps import AppConfig
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

logger = logging.getLogger('django')


class AmiHookConfig(AppConfig):
    name = 'ami_hook'
    verbose_name = _('Hooks AMI')

    def __init__(self, *args, **kwargs):
        super(AmiHookConfig, self).__init__(*args, **kwargs)
        self.ami_client = None

    def ready(self):
        ami_settings = getattr(settings, 'ASTERISK_AMI', {})
        if self.ami_client is not None:
            return
        try:
            self.ami_client = AMIClient(
                address=ami_settings.hostname,
                port=ami_settings.port,
            )
            f = self.ami_client.login(
                username=ami_settings.username,
                secret=ami_settings.password
            )
            response = f.response
            if response is None or response.is_error():
                logger.warning(text_type(response))
            from . import signals
        except Exception as ex:
            logger.warning(text_type(ex))

    def __del__(self):
        self.ami_client.logoff()
