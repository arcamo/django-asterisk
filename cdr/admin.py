from django.contrib import admin
from .models import CDR


@admin.register(CDR)
class CDRAdmin(admin.ModelAdmin):
    search_fields = ['clid', 'src', 'dst', ]
    list_filter = ['disposition']
    list_display = ['uniqueid', 'clid', 'dst', 'disposition']
