from collections import OrderedDict


def get_field_type(connection, row):
    field_params = OrderedDict()
    field_notes = []

    try:
        field_type = connection.introspection.get_field_type(row[1], row)
    except KeyError:
        field_type = 'TextField'
        field_notes.append('This field type is a guess.')

    # This is a hook for data_types_reverse to return a tuple of
    # (field_type, field_params_dict).
    if type(field_type) is tuple:
        field_type, new_params = field_type
        field_params.update(new_params)

    # Add max_length for all CharFields.
    if field_type == 'CharField' and row[3]:
        field_params['max_length'] = int(row[3])

    if field_type == 'DecimalField':
        if row[4] is None or row[5] is None:
            field_notes.append(
                'max_digits and decimal_places have been guessed, as this '
                'database handles decimal fields as float')
            field_params['max_digits'] = row[4] if row[4] is not None else 10
            field_params['decimal_places'] = row[5] if row[5] is not None else 5
        else:
            field_params['max_digits'] = row[4]
            field_params['decimal_places'] = row[5]

    return field_type, field_params, field_notes
