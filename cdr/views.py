import django_filters
from django.conf.urls import url
from django.db import models
from django_filters.rest_framework import FilterSet
from rest_framework import generics
from rest_framework import serializers
from .models import CDR

filter_meta = {}
filter_fields = [f.name for f in CDR._meta.get_fields()]
for field in CDR._meta.get_fields():
    if isinstance(field, (models.IntegerField, models.FloatField, models.DecimalField,)):
        field_filter_name = 'max_%s' % field.name
        filter_meta[field_filter_name] = django_filters.NumberFilter(name=field.name, lookup_expr='lte')
        filter_fields.append(field_filter_name)

        field_filter_name = 'min_%s' % field.name
        filter_meta[field_filter_name] = django_filters.NumberFilter(name=field.name, lookup_expr='gte')
        filter_fields.append(field_filter_name)

    elif isinstance(field, (models.DateTimeField, models.DateField,)):
        field_filter_name = 'max_%s' % field.name
        filter_meta[field_filter_name] = django_filters.DateFilter(name=field.name, lookup_expr='lte')
        filter_fields.append(field_filter_name)

        field_filter_name = 'min_%s' % field.name
        filter_meta[field_filter_name] = django_filters.DateFilter(name=field.name, lookup_expr='gte')
        filter_fields.append(field_filter_name)

    elif isinstance(field, (models.CharField, models.TextField,)):
        field_filter_name = '%s_contains' % field.name
        filter_meta[field_filter_name] = django_filters.CharFilter(name=field.name, lookup_expr='icontains')
        filter_fields.append(field_filter_name)

        field_filter_name = '%s_startswith' % field.name
        filter_meta[field_filter_name] = django_filters.CharFilter(name=field.name, lookup_expr='istartswith')
        filter_fields.append(field_filter_name)

        field_filter_name = '%s_endswith' % field.name
        filter_meta[field_filter_name] = django_filters.CharFilter(name=field.name, lookup_expr='iendswith')
        filter_fields.append(field_filter_name)

filter_meta['Meta'] = type('Meta', (object,), {'model': CDR, 'fields': filter_fields})

CDRFilter = type('CDRFilter', (FilterSet,), filter_meta)


class CDRSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CDR
        fields = tuple([f.name for f in CDR._meta.get_fields()])


class CDRListView(generics.ListAPIView):
    queryset = CDR.objects.all()
    serializer_class = CDRSerializer
    filter_class = CDRFilter
    ordering_fields = '__all__'
    ordering = ('-calldate',)


class CDRDetailVew(generics.RetrieveAPIView):
    queryset = CDR.objects.all()
    serializer_class = CDRSerializer


urlpatterns = [
    url(r'^cdr/$', CDRListView.as_view()),
    url(r'^cdr/(?P<pk>[^/]+)/$', CDRDetailVew.as_view()),
]
