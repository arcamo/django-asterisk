from __future__ import unicode_literals

from django.db import connections
from django.db import models
from django.utils.encoding import force_text
from .meta import *

meta_cdr = {
    'Meta': type(b'Meta', (object,), {
        'managed': False,
        'db_table': b'cdr',
        'ordering': ['-calldate']
    }),
    '__module__': b'cdr.models',
    'uniqueid': dict(primary_key=True),
}

connection = connections['cdr']
tables_to_introspect = ['cdr']
with connection.cursor() as cursor:
    known_models = []
    for table_name in tables_to_introspect:
        try:
            try:
                relations = connection.introspection.get_relations(cursor, table_name)
            except NotImplementedError:
                relations = {}
            try:
                constraints = connection.introspection.get_constraints(cursor, table_name)
            except NotImplementedError:
                constraints = {}
            primary_key_column = connection.introspection.get_primary_key_column(cursor, table_name)
            unique_columns = [c['columns'][0] for c in constraints.values() if c['unique'] and len(c['columns']) == 1]
            table_description = connection.introspection.get_table_description(cursor, table_name)
        except Exception as e:
            print "# Unable to inspect table '%s'" % table_name
            print "# The error was: %s" % force_text(e)
            continue

        for row in table_description:
            extra_params = meta_cdr.get(row.name, {})
            field_type, field_params, field_notes = get_field_type(connection, row)
            field_type = getattr(models, field_type)
            field_params.update(extra_params)
            meta_cdr[row.name] = field_type(**field_params)

CDR = type(b'CDR', (models.Model,), meta_cdr)
