# -*- encoding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from .models import Request, Attempt


@admin.register(Request)
class RequestAdmin(admin.ModelAdmin):
    list_display = ['created', 'url', 'method', 'success', 'attempts_count', 'next_attempt', ]
    list_filter = ['url', 'method']
    search_fields = ['url', 'method', 'headers', 'body']
    ordering = ['-created']

    def get_queryset(self, request):
        queryset = super(RequestAdmin, self).get_queryset(request)
        return queryset.extra_data()

    def attempts_count(self, obj):
        return obj.attempts_count

    attempts_count.short_description = _('Tentativas')
    attempts_count.admin_order_field = 'attempts_count'



@admin.register(Attempt)
class AttemptAdmin(admin.ModelAdmin):
    list_display = ['created', 'request', 'response_status', ]
    list_filter = ['response_status', 'request__url', 'request__method']
    search_fields = ['headers', 'body']
    ordering = ['-created']
