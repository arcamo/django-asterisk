# -*- encoding: utf-8 -*-
from __future__ import unicode_literals

import logging
import multiprocessing
import threading

from django.apps import AppConfig
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

logger = logging.getLogger('django')


def execute_request(pk):
    from .models import Request
    return Request.objects.get(pk=pk).execute()


def execute_callback(objs):
    try:
        map(lambda o: o.save(), objs)
    except:
        return None


class RequestsThread(object):
    def __init__(self):
        self._pool = multiprocessing.Pool(processes=multiprocessing.cpu_count() * 2)
        self._finished = threading.Event()
        self._wait = threading.Event()
        self._thread = threading.Thread(target=self.run)
        self._thread.daemon = True

    def start(self):
        self._thread.start()

    def run(self):
        from .models import Request

        while not self._finished.is_set():
            requests = list(Request.objects.pending())
            while len(requests) > 0:
                processes = [self._pool.apply_async(execute_request, [request.pk]) for request in requests]
                map(lambda p: p.wait(), processes)
                requests = list(Request.objects.pending())

            next_attempt = Request.objects.next_attempt()
            if next_attempt:
                dt = next_attempt - timezone.now()
                wait_time = dt.total_seconds() if dt.total_seconds() > 0 else 5
            else:
                wait_time = 5
            self._wait.wait(wait_time)
            self._wait.clear()

        return True

    def send_requests(self):
        self._wait.set()

    def stop(self):
        self._finished.set()

    def __del__(self):
        self.stop()


class RequestsQueueConfig(AppConfig):
    name = 'requests_queue'
    verbose_name = _('Fila de Requisições')

    def __init__(self, *args, **kwargs):
        super(RequestsQueueConfig, self).__init__(*args, **kwargs)
        self.requests_poll = None

    def ready(self):
        self.requests_poll = RequestsThread()
        self.requests_poll.start()
        try:
            from . import signals
        except:
            pass

    def __del__(self):
        self.requests_poll.stop()
