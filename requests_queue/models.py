# -*- encoding: utf-8 -*-
from __future__ import unicode_literals

import jsonfield
import requests
from django.db import models
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _


class RequestQuerySet(models.QuerySet):
    def extra_data(self):
        return self.annotate(
            attempts_count=models.Count('attempts'),
        )

    def next_attempt(self):
        return self.filter(
            success=None
        ).aggregate(models.Min('next_attempt'))['next_attempt__min']

    def pending(self):
        return self.filter(
            models.Q(next_attempt=None) | models.Q(next_attempt__lte=timezone.now()),
            success=None
        )


@python_2_unicode_compatible
class Request(models.Model):
    class Meta:
        verbose_name = _(u'Requisição')
        verbose_name_plural = _(u'Requisições')
        ordering = ['-created']

    objects = RequestQuerySet().as_manager()

    created = models.DateTimeField(
        auto_now_add=True,
        verbose_name=_('Criado em')
    )

    url = models.URLField(
        null=False,
        blank=False,
    )

    method = models.CharField(
        null=False,
        blank=False,
        max_length=32,
        choices=(
            ('POST', 'POST'),
            ('PUT', 'PUT'),
            ('GET', 'GET'),
            ('DELETE', 'DELETE'),
        ),
        verbose_name=_('Método')
    )

    headers = jsonfield.JSONField(
        null=True,
        blank=True,
        verbose_name=_('Cabeçario')
    )

    body = models.TextField(
        null=True,
        blank=True,
        verbose_name=_('Corpo')
    )

    next_attempt = models.DateTimeField(
        null=True,
        blank=True,
        verbose_name=_('Próxima tentativa')
    )
    success = models.DateTimeField(
        null=True,
        blank=True,
        verbose_name=_('Sucesso')
    )

    def __str__(self):
        return self.method + '<' + str(self.pk) + '>:' + self.url + ' ' + str(self.created)

    def execute(self):
        method = self.method.lower()
        try:
            r = getattr(requests, method)(self.url, data=self.body, headers=self.headers)
            attempt = Attempt(response_status=r.status_code, headers=dict(r.headers.items()), body=r.text, request=self)
            if 200 <= r.status_code < 300:
                self.success = timezone.now()
        except Exception as ex:
            attempt = Attempt(response_status=-1, log=str(ex), request=self).save()
        seconds = 2 ** min(self.attempts.count(), 18)
        self.next_attempt = timezone.now() + timezone.timedelta(seconds=seconds)
        self.save()
        attempt.save()
        return attempt, self


class Attempt(models.Model):
    class Meta:
        verbose_name = _(u'Tentativa')
        verbose_name_plural = _(u'Tentativas')
        ordering = ['-created']

    created = models.DateTimeField(
        auto_now_add=True,
        verbose_name=_('Criada em')
    )

    request = models.ForeignKey(
        Request,
        null=False,
        blank=False,
        related_name='attempts',
        verbose_name=_('Requisição'),
    )

    response_status = models.IntegerField(
        null=False,
        blank=False,
        verbose_name=_('Status da resposta')
    )

    headers = jsonfield.JSONField(
        null=True,
        blank=True,
        verbose_name=_('Cabeçario da resposta')
    )

    body = models.TextField(
        null=True,
        blank=True,
        verbose_name=_('Corpo da resposta')
    )

    log = models.TextField(
        null=True,
        blank=True,
        verbose_name=_('Erro')
    )
