default_app_config = 'requests_queue.apps.RequestsQueueConfig'


def post(url, headers, raw_body):
    from .models import Request
    request = Request(
        method='POST',
        url=url,
        headers=headers,
        body=raw_body
    )
    request.save()


def put(url, headers, raw_body):
    from .models import Request
    request = Request(
        method='PUT',
        url=url,
        headers=headers,
        body=raw_body
    )
    request.save()
