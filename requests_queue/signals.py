from django.apps import apps
from django.db.models.signals import post_save
from django.dispatch import receiver
from .models import Request

requests_poll = apps.get_app_config('requests_queue').requests_poll


@receiver(post_save, sender=Request)
def post_save_request(instance, created, **kwargs):
    if not created:
        return
    requests_poll.send_requests()
