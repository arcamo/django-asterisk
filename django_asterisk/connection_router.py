from django.db import connections


class ConnectionRouter(object):
    def db_for_read(self, model, **hints):
        if model._meta.app_label in connections:
            return model._meta.app_label
        return 'default'

    def db_for_write(self, model, **hints):
        if model._meta.app_label in connections:
            return model._meta.app_label
        return 'default'
