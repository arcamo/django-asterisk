CORES ?= $(shell grep -c ^processor /proc/cpuinfo)
ADDRESS ?= 0.0.0.0
PORT ?= 8000
WORKERS ?= 1
CLIENTS ?= 1000
ACCESS_LOG ?= access.log
ERROR_LOG ?= error.log
KEEP_ALIVE ?= 2
TIMEOUT ?= 30
PYTHON  ?= python
WSGI ?= django_asterisk.wsgi:application

init: venv pip

venv:
	test -d venv || virtualenv -p $(PYTHON) venv
	venv/bin/pip install --upgrade pip

clean:
	rm -f $(shell find . -name "*.pyc")
	rm -f $(shell find . -name "*.log")
	rm -rf .coverage
	rm -rf htmlcov
	rm -rf static

pip: requirements.txt venv
	venv/bin/pip install -Ur requirements.txt

pip-dev: requirements-dev.txt venv
	venv/bin/pip install -Ur requirements-dev.txt

test: pip pip-dev
	venv/bin/python manage.py test

database:
	venv/bin/python manage.py makemigrations --no-input
	venv/bin/python manage.py migrate --no-input

static:
	venv/bin/python manage.py collectstatic --no-input

run: pip database clean static
	venv/bin/gunicorn $(WSGI)

start: pip database clean static
	venv/bin/gunicorn $(WSGI) -D -w $(WORKERS) -b $(ADDRESS):$(PORT) --preload --pid pid.txt --worker-connections $(CLIENTS) --keep-alive $(KEEP_ALIVE) -t $(TIMEOUT) --access-logfile $(ACCESS_LOG) --error-logfile $(ERROR_LOG)

kill: pid.txt
	kill -7 $(shell cat pid.txt) || true
